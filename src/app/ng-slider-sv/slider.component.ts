import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'ng-slider-sv',
  templateUrl: './slider.component.html',
  styleUrls: [
    './slider.component.css',
  ]
})
export class SliderComponent {
  public slide: number = 0;
  public offset: number = 0;
  public total: number = 3;
  public leftDisabled: boolean = true;
  public rightDisabled: boolean = this.total === 1;
  public pause$: BehaviorSubject<any> = new BehaviorSubject(false);
  public newSlide$: Observable<any> = new Observable<any>();
  public interval$: Observable<any> = new Observable<any>();
  
  @Input()
  public transition_opts: string = '.5s ease-in-out';
  
  @Input()
  public interval: number = 5000;
  
  @Input()
  images: any;
  
  
  ngAfterViewInit() {
    setTimeout(() => this.calc(), 1);
    
    this.interval$ = Observable
    .interval(5000)
    .map((s) => Observable.of(true));
    this.interval$.subscribe();
    
    this.newSlide$ = this.interval$
    .withLatestFrom(this.pause$)
    .filter(([i, p]) => {
      return !p;
    })
    .switchMap(([i, p]) => i);
    this.newSlide$.subscribe(() => this.goNext(false));
    
  }
  
  playPause() {
    this.pause$.next(!this.pause$.getValue());
  }
  
  goBack(e) {
    if (e) {
      e.stopPropagation();
    }
    if (this.slide > 0) {
      this.slide--;
      this.calc();
    }
    this.pause$.next(true);
  }
  
  goNext(e) {
    if (e) {
      e.stopPropagation();
      this.pause$.next(true);
    }
    if (this.slide < (this.total - 1)) {
      this.slide++;
    } else {
      this.slide = 0;
    }
    this.calc();
  }
  
  calc() {
    for (let data of this.images) {
      data.active = false;
    }
    this.images[this.slide].active = true;
    
    if (this.slide > 0) {
      this.leftDisabled = false;
    } else {
      this.leftDisabled = true;
    }
    if (this.slide < (this.total - 1)) {
      this.rightDisabled = false;
    } else {
      this.rightDisabled = true;
    }
    this.offset = -this.slide * 100;
  }
  
  
}

//всплывание текста
// директива с настройками
// infinite